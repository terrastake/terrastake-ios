//
//  TerraStakeApp.swift
//  TerraStake
//
//  Created by Temur Chitashvili on 04.07.24.
//

import SwiftUI

@main
struct TerraStakeApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
