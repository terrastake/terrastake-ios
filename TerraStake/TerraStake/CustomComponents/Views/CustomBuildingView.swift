//
//  CustomBuildingView.swift
//  TerraStake
//
//  Created by Temur Chitashvili on 05.07.24.
//

import SwiftUI

struct  CustomBuildingView: View {
    @StateObject var imageLoader: ImageLoader
    
    let imageSize: CGFloat
    
    init(url: URL, imageSize: CGFloat) {
        self._imageLoader = StateObject(wrappedValue: ImageLoader(url: url))
        self.imageSize = imageSize
    }
    
    var body: some View {
        Group {
            if imageLoader.image != nil {
                Image(uiImage: imageLoader.image!)
                    .resizable()
                    .scaledToFill()
                    .frame(width: imageSize, height: imageSize)
                    .cornerRadius(10)
                    .clipped()
            } else if imageLoader.errorMessage != nil {
                Text(imageLoader.errorMessage!)
                    .foregroundColor(.red)
                    .frame(width: imageSize, height: imageSize)
            } else {
                ProgressView()
                    .frame(width: imageSize, height: imageSize)
            }
        }
        .onAppear {
            imageLoader.fetchImage()
        }
    }
}
