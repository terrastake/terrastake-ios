//
//  CustomTextField.swift
//  TerraStake
//
//  Created by Temur Chitashvili on 04.07.24.
//

import SwiftUI

struct CustomTextField: View {
    let textFieldTitle: String
    
    @Binding var textFieldText: String
    
    var body: some View {
        VStack(alignment: .leading) {
            Text(textFieldTitle)
                .padding(.leading, 10)
            
            TextField(textFieldText,
                      text: $textFieldText)
            .textFieldStyle()
        }
        .padding(.horizontal, 16)
    }
}
