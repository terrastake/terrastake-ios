//
//  SwiftUIView.swift
//  TerraStake
//
//  Created by Temur Chitashvili on 04.07.24.
//

import SwiftUI

struct CustomTabView: View {
    var body: some View {
        TabView {
            ProjectsListView()
                .tabItem {
                    Image(systemName: "house")
                    Text("მთავარი")
                }
            
            ProfileView()
                .tabItem {
                    Image(systemName: "person")
                    Text("პროფილი")
                }
        }
        .accentColor(.customLabel)
    }
}

#Preview {
    CustomTabView()
}
