//
//  InvestAProjectSheet.swift
//  TerraStake
//
//  Created by Temur Chitashvili on 06.07.24.
//

import SwiftUI

struct InvestAProjectSheet: View {
    @StateObject var detailsViewModel = ProjectDetailsViewModel()
    
    let project: Project
    
    var body: some View {
        NavigationView(content: {
            content
                .background(Color.customBackground)
                .navigationTitle("ინვესტირება")
        })
    }
    
    private var content: some View {
        userInfoCardView
    }
    
    private var userInfoCardView: some View {
        ZStack {
            walletInfoBackground
            
            VStack {
                Text("TerraWallet")
                    .font(.title)
                    .bold()
                
                Text("გაითვალისწინეთ, მინიმალური შენატანი უნდა იყოს " + detailsViewModel.getMinAmountToInvest(project.price) + "$")
                
                userInfoContent
                    
                Spacer()
            }
        }
        .padding(.horizontal, 24)
        .padding(.top, 20)
    }
    
    private var walletInfoBackground: some View {
        RoundedRectangle(cornerRadius: 16)
            .frame(maxWidth: .infinity,
                   minHeight: 0)
            .foregroundStyle(Color.customCard)
            .padding(.bottom, -10)
    }
    
    private var userInfoContent: some View {
        VStack {
            CustomTextField(textFieldTitle: "თანხა",
                            textFieldText: $detailsViewModel.moneyForInvest)
            
            Text("თქვენ მიიღებთ - " + detailsViewModel.getNftQuantity() + " NFT - ს")
            
            Button(action: {
                detailsViewModel.buyAnInvestment(project)
            }, label: {
                Text("ყიდვა")
                    .foregroundStyle(Color.customLabel)
                    .padding(12)
            })
            .background(
                RoundedRectangle(cornerRadius: 30)
                    .foregroundStyle(Color.customCard)
            )
        }
        .padding(.top, 10)
    }
}
