//
//  ProjectDetailsViewModel.swift
//  TerraStake
//
//  Created by Temur Chitashvili on 05.07.24.
//

import Foundation

final class ProjectDetailsViewModel: ObservableObject {
    @Published var moneyForInvest: String = ""
    var minAmount = 0
    
    func getMinAmountToInvest(_ projectPrice: Int) -> String{
        minAmount = projectPrice / 10
        return String(minAmount)
    }
    
    func getNftQuantity() -> String {
        let nftQuantity = (Int(moneyForInvest) ?? 0)
        let nftQuantitySecond = nftQuantity / minAmount
        
        return String(nftQuantitySecond)
    }
    
    func buyAnInvestment(_ project: Project) {
        mockUser.projectsInvested.append(project)
    }
}
