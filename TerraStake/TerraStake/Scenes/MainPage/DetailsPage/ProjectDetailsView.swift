//
//  ProjectDetailsView.swift
//  TerraStake
//
//  Created by Temur Chitashvili on 05.07.24.
//

import SwiftUI

struct ProjectDetailsView: View {
    @Binding var navPath: [Int]
    @State var investSheetIsOpen: Bool = false
    
    let project: Project
    
    var body: some View {
        content
            .background(Color.customBackground)
    }
    
    private var content: some View {
        VStack {
            projectImagesGallery
            
            projectInfoCardView
            
            makeAnInvestmentButton
            
            Spacer()
        }
    }
    
    
    private var projectImagesGallery: some View {
        ScrollView(.horizontal) {
            scrollableImagesContent
        }
        .scrollIndicators(.hidden)
        .frame(width: 350, height: 350)
    }
    
    private var scrollableImagesContent: some View {
        LazyHStack {
            ForEach(0..<project.images.count) { index in
                let image = project.images[index]
                CustomBuildingView(url: URL(string: image)!, imageSize: 330)
            }
        }
    }
    
    private var projectInfoCardView: some View {
        ZStack {
            projectInfoBackground
            
            projectInfoContent
        }
        .padding(.horizontal, 24)
        .padding(.top, 10)
    }
    
    private var projectInfoBackground: some View {
        RoundedRectangle(cornerRadius: 16)
            .frame(maxWidth: .infinity,
                   minHeight: 0)
            .foregroundStyle(Color.customCard)
            .padding(.bottom, -10)
    }
    
    private var projectInfoContent: some View {
        VStack(alignment: .leading,spacing: 10) {
            projectDescription
            
            projectLocation
            
            projectCost
            
            projectArea
            
            projectAuthorName
        }
        .foregroundStyle(Color.customLabel)
        .padding(.horizontal, 6)
    }
    
    private var projectDescription: some View {
        Text(project.description)
            .font(.system(size: 14, weight: .regular))
    }
    
    private var projectLocation: some View {
        Text("ლოკაცია: " + project.location)
            .font(.system(size: 12, weight: .regular))
    }
    
    private var projectCost: some View {
        Text("ღირებულება: " + String(project.price) + "$")
            .font(.system(size: 12, weight: .regular))
    }
    
    private var projectArea: some View {
        Text("ფართობი: " + String(project.area) + " კვ.მ.")
            .font(.system(size: 12, weight: .regular))
    }
    
    private var projectAuthorName: some View {
        Text("გამომქვეყნებელი: " + String(project.authorName))
            .font(.system(size: 12, weight: .regular))
    }
    
    private var makeAnInvestmentButton: some View {
        HStack {
            Spacer()
            
            Button(action: {
                investSheetIsOpen.toggle()
            }, label: {
                Text("ინვესტირება")
                    .foregroundStyle(Color.customLabel)
                    .padding(12)
            })
            .background(
                RoundedRectangle(cornerRadius: 30)
                    .foregroundStyle(Color.customCard)
            )
            .sheet(isPresented: $investSheetIsOpen) {
                InvestAProjectSheet(project: project)
                    .presentationDetents([.medium])
            }
            .padding(.top, 16)
        }
        .padding(.trailing, 24)
    }
}


