//
//  ProjectsListViewModel.swift
//  TerraStake
//
//  Created by Temur Chitashvili on 04.07.24.
//

import Foundation
import Network

final class ProjectsListViewModel: ObservableObject {
    private let projectApi: String = "https://mocki.io/v1/ece0be34-93bb-42f1-926a-6c4b417d89fa"
    @Published var searchForCity = ""
    @Published var projects: [Project] = []
    
    init() {
       fetchData()
    }
    
    
    func getProject(withId id: Int) -> Project {
        projects.first(where: {$0.id == id})!
    }
    
    var filteredProjects: [Project] {
        switch searchForCity {
        case "" :
            return projects
        default :
            return projects.filter({$0.location.localizedCaseInsensitiveContains(searchForCity)})
        }
    }
    
    private func fetchData() {
        NetworkService.networkService.getData(urlString: projectApi) { [weak self]  (result: Result<[Project], Error>) in
            DispatchQueue.main.async { [weak self] in
                switch result {
                case .success(let success):
                    self?.projects = success
                case .failure(let failure):
                    print(failure.localizedDescription)
                }
            }
        }
    }
}
