//
//  ProjectsListView.swift
//  TerraStake
//
//  Created by Temur Chitashvili on 04.07.24.
//

import SwiftUI

struct ProjectsListView: View {
    @StateObject var projectsListViewModel =  ProjectsListViewModel()
    
    @State var navPath: [Int] = []
    
    var body: some View {
        NavigationStack(path: $navPath) {
            ScrollView {
                ForEach(projectsListViewModel.filteredProjects) { project in
                    NavigationLink(value: project.id) {
                        ProjectRow(project: project)
                    }
                }
                .navigationTitle("განცხადებები")
                .navigationDestination(for: Int.self) { id in
                    let project = projectsListViewModel.getProject(withId: id)
                    ProjectDetailsView(navPath: $navPath, project: project)
                }
                .searchable(text: $projectsListViewModel.searchForCity)
            }
            .padding(.top, 10)
            .background(Color.customBackground)
        }
    }
}

#Preview {
    ProjectsListView()
}
