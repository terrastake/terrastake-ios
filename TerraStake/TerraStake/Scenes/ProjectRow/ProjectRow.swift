//
//  ProjectRow.swift
//  TerraStake
//
//  Created by Temur Chitashvili on 06.07.24.
//

import SwiftUI

struct ProjectRow: View {
    let project: Project
    
    var body: some View {
        content
            .padding(.horizontal, 16)
    }
    
    private var content: some View {
        HStack {
            projectImagesGallery
            
            projectInfo
        }
    }
    
    private var projectImagesGallery: some View {
        ScrollView(.horizontal) {
            LazyHStack {
                ForEach(0..<project.images.count) { index in
                    let image = project.images[index]
                    CustomBuildingView(url: URL(string: image)!, imageSize: 130)
                }
            }
        }
        .scrollIndicators(.hidden)
        .frame(width: 150, height: 150)
    }
    
    private var projectInfo: some View {
        VStack {
            projectDescriptionContent
            
            Spacer()
        }
    }
    
    private var projectDescriptionContent: some View {
        VStack(alignment: .leading) {
            projectDescription
            
            seeMoreButton
         
            locationText
        }
        .padding(.top, 6)
    }
    
    private var projectDescription: some View {
        Text(project.description)
            .foregroundStyle(Color.customLabel)
            .multilineTextAlignment(.leading)
            .lineLimit(2)
    }
    
    private var seeMoreButton: some View {
        HStack {
            Text("მეტი..")
                .foregroundStyle(Color.customCard)
            
            Spacer()
        }
    }
    
    private var locationText: some View {
        Text(project.location)
            .foregroundStyle(Color.customLabel)
            .padding(6)
            .background(
                RoundedRectangle(cornerRadius: 30)
                    .foregroundStyle(Color.customCard)
            )
            .padding(.top, 6)
    }
}
