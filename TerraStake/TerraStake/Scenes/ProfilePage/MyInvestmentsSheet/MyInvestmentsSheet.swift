//
//  MyInvestmentsSheet.swift
//  TerraStake
//
//  Created by Temur Chitashvili on 05.07.24.
//

import SwiftUI

struct MyInvestmentsSheet: View {
    @EnvironmentObject var profileViewModel: ProfileViewModel
    
    var body: some View {
        NavigationStack {
            Group {
                if !mockUser.projectsInvested.isEmpty {
                    emptyInvestmentsView
                } else {
                    filledInvestmentsView
                }
            }
            .navigationTitle("ჩემი ინვესტიციები")
        }
    }
    
    private var emptyInvestmentsView: some View {
        ScrollView {
            ForEach(mockUser.projectsInvested) { project in
                ProjectRow(project: project)
            }
        }
        .padding(.top, 10)
        .background(Color.customBackground)
    }
    
    private var filledInvestmentsView: some View {
        VStack {
            Image(.logo)
                .resizable()
                .frame(width: 300, height: 300)
                .scaledToFill()
            
            Text("თქვენ არ გაქვთ ინვესტიციები,\nდაიწყეთ ახლავე.")
                .font(.title)
                .multilineTextAlignment(.center)
        }
    }
}
