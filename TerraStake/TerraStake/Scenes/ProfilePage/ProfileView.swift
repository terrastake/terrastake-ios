//
//  ProfileView.swift
//  TerraStake
//
//  Created by Temur Chitashvili on 04.07.24.
//

import SwiftUI

struct ProfileView: View {
    @EnvironmentObject var profileViewModel: ProfileViewModel
    
    @State var investSheetIsOpen: Bool = false
    @State var personalInfoChanged: Bool = true
    
    var body: some View {
        content
            .background(Color.customBackground)
    }
    
    private var content: some View {
        VStack {
            profileImageAndUserFullName
            
            userInfoCardView
            
            changePersonalDataButton
          
            myInvestmentsButton
        }
    }
    
    private var profileImageAndUserFullName: some View {
        HStack {
            profileImage
            
            userFullName
            
            Spacer()
        }
        .padding(.leading, 24)
    }
    
    private var profileImage: some View {
        Image(systemName: "person.circle.fill")
            .resizable()
            .frame(width: 80,
                   height: 80)
            .padding(.top, 20)
    }
    
    private var userFullName: some View {
        Text(profileViewModel.firstName + "\n" + profileViewModel.lastName)
            .font(.system(size: 22,
                          weight: .regular))
            .foregroundStyle(Color.customLabel)
            .padding(.leading, 6)
    }
    
    private var userInfoCardView: some View {
        ZStack {
            userInfoBackground
            
            userInfoContent
        }
        .padding(.horizontal, 24)
        .padding(.top, 70)
    }
    
    private var userInfoBackground: some View {
        RoundedRectangle(cornerRadius: 16)
            .frame(maxWidth: .infinity,
                   minHeight: 0)
            .foregroundStyle(Color.customCard)
            .padding(.bottom, -10)
    }
    
    private var userInfoContent: some View {
        VStack {
            Text("პირადი ინფორმაცია")
                .font(.system(size: 18))
                .padding(10)
            
            CustomTextField(textFieldTitle: "სახელი",
                            textFieldText: $profileViewModel.firstName)
            
            CustomTextField(textFieldTitle: "გვარი",
                            textFieldText: $profileViewModel.lastName)
            
            CustomTextField(textFieldTitle: "მეილი",
                            textFieldText: $profileViewModel.email)
            
            Spacer()
        }
        .disabled(personalInfoChanged)
        .padding(.top, 10)
    }
        
    private var changePersonalDataButton: some View {
        Button(action: {
            personalInfoChanged.toggle()
        }, label: {
            Text(personalInfoChanged ? "შეცვალე ინფორმაცია" : "შეინახე ცვლილებები")
                .foregroundStyle(Color.customLabel)
                .padding(12)
        })
        .background(
            RoundedRectangle(cornerRadius: 30)
                .foregroundStyle(Color.customCard)
        )
        .padding(.top, 30)
    }
    
    private var myInvestmentsButton: some View {
        VStack {
            Spacer()
            
            HStack {
                Spacer()
                
                Button(action: {
                    investSheetIsOpen.toggle()
                }, label: {
                    Text("ჩემი ინვესტიციები")
                        .foregroundStyle(Color.customLabel)
                        .padding(12)
                })
                .background(
                    RoundedRectangle(cornerRadius: 30)
                        .foregroundStyle(Color.customCard)
                )
                .sheet(isPresented: $investSheetIsOpen) {
                    MyInvestmentsSheet()
                }
                .padding(.top, 16)
            }
        }
        .padding(.trailing, 24)
    }
}

#Preview {
    ContentView()
        .environmentObject(ProfileViewModel())
}
