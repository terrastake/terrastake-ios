//
//  ProfileViewModel.swift
//  TerraStake
//
//  Created by Temur Chitashvili on 04.07.24.
//

import Foundation

final class ProfileViewModel: ObservableObject {
    @Published var firstName: String = mockUser.name
    @Published var lastName: String = mockUser.lastName
    @Published var email: String = mockUser.email
}
