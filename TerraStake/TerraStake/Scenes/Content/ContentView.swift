//
//  ContentView.swift
//  TerraStake
//
//  Created by Temur Chitashvili on 04.07.24.
//

import SwiftUI

struct ContentView: View {
    @StateObject var profileViewModel = ProfileViewModel()
    
    var body: some View {
        CustomTabView()
            .environmentObject(profileViewModel)
    }
}

#Preview {
    ContentView()
}
