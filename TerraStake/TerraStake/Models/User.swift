//
//  User.swift
//  TerraStake
//
//  Created by Temur Chitashvili on 05.07.24.
//

import Foundation

struct User: Identifiable {
    let id: Int
    let name: String
    let lastName: String
    let email: String
    var projectsInvested: [Project]
}

var mockUser: User = User(id: 0, name: "ნიკოლოზ", lastName: "გელაშვილი", email: "nikol.gelashvili@gmail.com", projectsInvested: [])
