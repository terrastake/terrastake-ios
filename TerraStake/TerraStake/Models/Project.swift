//
//  Project.swift
//  TerraStake
//
//  Created by Temur Chitashvili on 04.07.24.
//

import UIKit


struct Project: Identifiable, Decodable {
    let id: Int
    let authorName: String
    let location: String
    let description: String
    let images: [String]
    let price: Int
    let area: Int
}
